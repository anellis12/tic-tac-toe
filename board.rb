class Board
  attr_reader  :comp_hits, :user_hits, :covered_coordinates, :winning_lines
  def initialize
    @covered_coordinates = []
    @comp_hits = []
    @user_hits = []
    @winning_lines = [
      [[1,1],[1,2],[1,3]],
      [[2,1],[2,2],[2,3]],
      [[3,1],[3,2],[3,3]],
      [[1,1],[2,1],[3,1]],
      [[1,2],[2,2],[3,2]],
      [[1,3],[2,3],[3,3]],
      [[1,1],[2,2],[3,3]],
      [[3,1],[2,2],[1,3]]
      ]
  end

  def display
    puts "\n"
    puts "    a   b   c"
    puts "  -------------"
    (1..3).each do |y|
      output_row = "#{y} |"
      (1..3).each do |x|
        if @comp_hits.include?([x,y])
          output_row += " O |"
        elsif @user_hits.include?([x,y])
          output_row += " X |"
        else
          output_row += "   |"
        end
      end
      puts output_row
      puts "  -------------"
    end
  end

  def covered?(x, y)
    @covered_coordinates = @comp_hits + @user_hits
    if @covered_coordinates.include?([x,y])
      covered = true
    else
      covered = false
    end
    covered
  end

  def user_close?
    true unless !partial_line("user") || partial_line("user").empty?
  end

  def partial_line(player)
    @partial_line = []
    @winning_lines.each do |line|
      @player_partial_line = []
      @already_blocked = false
      line.each do |space|
        find_partial_line_for_player(space, player)
      end
      if @player_partial_line.length == 2 && !@already_blocked
        @partial_line << @player_partial_line
      end
    end
    @partial_line
  end

  def find_partial_line_for_player(space, player)
    if player == "user"
      if comp_hits.include?(space)
        @player_partial_line = []
        @already_blocked = true
      else
        if user_hits.include?(space)
          @player_partial_line << space
        end
      end
    elsif player == "computer"
      if user_hits.include?(space)
        @player_partial_line = []
        @already_blocked = true
      else
        if comp_hits.include?(space)
          @player_partial_line << space
        end
      end
    end
  end

end
