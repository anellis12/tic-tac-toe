class Computer
  def initialize(board)
    @board = board
  end

  def place
    if !@board.covered?(2,2)
      @board.comp_hits << [2,2]
    elsif can_win?
      win
    elsif @board.user_close?
      block_user_spot
    elsif corner_available? && !user_fork_possible?
      choose_corner
    else
      choose_randomly
    end
  end

  def can_win?
    true unless @board.partial_line("computer").empty?
  end

  def find_best_spot(first_coordinate, second_coordinate)
    make_line = @board.winning_lines.select{ |line| line.include?(first_coordinate) && line.include?(second_coordinate)}
    spot = make_line.first.select{ |space| space != first_coordinate && space != second_coordinate  }
    @spot = spot.first
  end

  def win
    cpu_partial_line = @board.partial_line("computer")
    cpu_partial_line.each do |partial_line|
      first_coordinate = partial_line[0]
      second_coordinate = partial_line[1]
      find_best_spot(first_coordinate, second_coordinate)
    end
    @board.comp_hits << @spot
  end

  def block_user_spot
    user_partial_line = @board.partial_line("user")
    user_partial_line.each do |partial_line|
      first_coordinate = partial_line[0]
      second_coordinate = partial_line[1]
      find_best_spot(first_coordinate, second_coordinate)
    end
    @board.comp_hits << @spot
  end

  def corner_available?
    @corner_available = false
    @corners = [[1,1], [1,3], [3,1], [3,3]]
    @corners.each do |corner|
      unless @board.covered?(corner[0], corner[1])
        @corner_available = true
      end
    end
    @corner_available
  end

  def choose_corner
    @chosen_corner = nil
    @board.user_hits.each do |uh|
      @corners.each do |c|
        if c == uh
          opposite_x = 3 if c[0] == 1
          opposite_x = 1 if c[0] == 3

          opposite_y = 3 if c[1] == 1
          opposite_y = 1 if c[1] == 3
          @chosen_corner = [opposite_x, opposite_y] unless @board.covered?(opposite_x,opposite_y)
        end
      end
    end
    unless @chosen_corner
      @corners.each do |c|
        unless @board.covered?(c[0], c[1])
          @chosen_corner = c
        end
      end
    end
    @board.comp_hits << @chosen_corner
  end


  def user_fork_possible?
    true if (@board.user_hits.include?([1,1]) && @board.user_hits.include?([3,3])) ||
      (@board.user_hits.include?([3,1]) && @board.user_hits.include?([1,3]))
  end

  def choose_randomly
    placed = placed || false
    @non_corners = [[2,1], [1,2], [3,2], [2,3]]
    @non_corners.each do |space|
      unless @board.covered?(space[0],space[1])
        @board.comp_hits << [space[0],space[1]] unless placed
        placed = true
      end
    end
  end



end
