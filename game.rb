require './tictactoe.rb'
require './board.rb'
require './user.rb'
require './computer.rb'


game = TicTacToe.new
puts "Hi, welcome to Tic Tac Toe! User is X and the computer is O"
puts "We'll flip a coin to determine who goes first. Heads or tails?"
choice = gets.chomp
if choice.downcase == "heads"
  choice = 1
else
  choice = 2
end
coin = rand(1..2)
if choice == coin
  puts "You go first!"
  game.board.display
  until game.draw? || game.won? || game.won?("user")
    game.play
  end
else
  puts "The computer goes first."
  game.play(computer_first = true)
  until game.draw? || game.won? || game.won?("user")
    game.play
  end
end
if game.won?("user")
  puts "Congratulations! You won!"
elsif game.won?
  puts "Sorry, the computer beat you"
else
  puts "It was a draw!"
end
