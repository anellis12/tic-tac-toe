class TicTacToe
  attr_reader :user, :computer
  attr_accessor :board
  def initialize
    @board = Board.new
    @user= User.new(@board)
    @computer = Computer.new(@board)
  end


  def play(computer_first = nil)
    unless won? || won?("user")|| draw? || computer_first
      puts "Please choose a spot. (Ex. a1)"
      choice = gets.chomp
      x, y = coordinates(choice)
      until user.place(x, y)
        puts "That is not a valid selection. Please choose again."
        choice = gets.chomp
        x, y = coordinates(choice)
      end
      board.display
    end
    computer.place
    show_computer_hit
    board.display
  end

  def show_computer_hit
    comp_x = "a" if board.comp_hits.last[0] == 1
    comp_x = "b" if board.comp_hits.last[0] == 2
    comp_x = "c" if board.comp_hits.last[0] == 3
    comp_y = board.comp_hits.last[1]
    puts "The computer chose #{comp_x}#{comp_y}"
  end

  def coordinates(choice)
    x = choice[0].downcase
    y = choice[1].to_i

    unless ['a', 'b', 'c'].include?(x) && [1,2,3].include?(y)
      puts "That is not a valid selection. Please choose again."
      choice = gets.chomp
      x, y = coordinates(choice)
    end
    x = 1 if x == "a"
    x = 2 if x == "b"
    x = 3 if x == "c"
    [x,y]
  end

  def draw?
    if board.comp_hits.length + board.user_hits.length >=9
      true
    end
  end

  def won?(player = "computer")
    @won = nil
    if player == "user"
      hits = board.user_hits
    else
      hits = board.comp_hits
    end
    board.winning_lines.each do |line|
      if hits.include?(line[0]) && hits.include?(line[1]) && hits.include?(line[2])
        @won = true
      end
    end
    @won
  end



end
