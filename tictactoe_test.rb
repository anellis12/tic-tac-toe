require 'minitest/autorun'
require 'minitest/pride'
require './tictactoe.rb'
require './board.rb'
require './user.rb'
require './computer.rb'

class TicTacToeTest < Minitest::Test

  def test_tictactoe_class_exists
    assert TicTacToe
  end

  def test_empty_board_can_display_itself
    board = Board.new
    assert_output(empty_board) do
      board.display
    end
  end

#I could not indent properly here because the indentation is interpreted as part of the string.
  def empty_board
    %Q{
    a   b   c
  -------------
1 |   |   |   |
  -------------
2 |   |   |   |
  -------------
3 |   |   |   |
  -------------
}
  end

  def test_user_can_hit
    game = TicTacToe.new
    user = game.user
    board = game.board
    assert user.place(2, 1)
    refute board.covered?(1, 1)
    assert board.covered?(2, 1)
  end

  def test_computer_can_hit
    game = TicTacToe.new
    board = game.board
    user = game.user
    computer = game.computer
    user.place(2, 1)
    computer.place
    x = board.comp_hits.last[0]
    y = board.comp_hits.last[1]
    assert board.covered?(x,y)
  end

  def test_game_can_be_a_draw
    game = TicTacToe.new
    user = game.user
    board = game.board
    computer = game.computer
    board.comp_hits << [1,1]
    board.comp_hits << [2,1]
    board.comp_hits << [3,1]
    board.comp_hits << [1,2]
    board.comp_hits << [1,3]
    board.comp_hits << [2,2]
    board.comp_hits << [2,3]
    board.comp_hits << [3,2]
    board.comp_hits << [3,3]
    assert game.draw?
  end

  def test_user_can_win_game
    game = TicTacToe.new
    board = game.board
    board.user_hits << [1,1]
    board.user_hits << [1,2]
    board.user_hits << [1,3]
    assert game.won?("user")
  end

  def test_cpu_can_win_game
    game = TicTacToe.new
    board = game.board
    board.comp_hits << [1,1]
    board.comp_hits << [1,2]
    board.comp_hits << [1,3]
    assert game.won?
  end

  def test_game_recognizes_when_user_is_close_to_winning
    game = TicTacToe.new
    board = game.board
    computer = game.computer
    board.user_hits << [1,1]
    computer.place
    board.user_hits << [1,3]
    assert board.user_close?

    board.user_hits << [3,2]
    computer.place
    board.user_hits << [1,3]
    refute board.user_close?
  end

  def test_game_blocks_user_winning_lines
    game = TicTacToe.new
    board = game.board
    computer = game.computer
    board.user_hits << [1,1]
    computer.place
    board.user_hits << [1,3]
    computer.place
    assert_equal [1,2], game.board.comp_hits.last
  end

  def test_game_knows_when_corner_is_avalaible
    game = TicTacToe.new
    board = game.board
    computer = game.computer
    board.user_hits << [1,1]
    computer.place
    assert computer.corner_available?

    board.user_hits << [1,3]
    board.user_hits << [3,3]
    board.user_hits << [3,1]
    refute computer.corner_available?
  end

end
