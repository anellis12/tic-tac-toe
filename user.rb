class User
  def initialize(board)
    @board = board
  end

  def place(x,y)
    if @board.user_hits.include?([x,y]) || @board.comp_hits.include?([x,y]) || x > 3 && y > 3
      false
    else
      @board.user_hits << [x,y]
    end
  end
end
